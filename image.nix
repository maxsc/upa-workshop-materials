{ pkgs, config, nix-filter, modulesPath, lib, ... }:

with lib;

{
  imports =
    [ "${modulesPath}/installer/cd-dvd/installation-cd-graphical-gnome.nix" ];

  # use the latest Linux kernel
  boot.kernelPackages = pkgs.linuxPackages_latest;

  # Needed for https://github.com/NixOS/nixpkgs/issues/58959
  boot.supportedFilesystems =
    lib.mkForce [ "btrfs" "reiserfs" "vfat" "f2fs" "xfs" "ntfs" "cifs" ];

  # Install Android SDK
  programs.adb.enable = true;
  users.users.nixos.extraGroups = [ "adbusers" ];

  # TODO Make repository contents available in user home folder
  # lib.isoFileSystems = {
  #   "/home/nixos/UpcyclingAndroid" = mkImageMediaOverride {
  #     device = "/iso/UpcyclingAndroid";
  #     fsType = "none";
  #     options = [ "bind" "defaults" ];
  #   };
  # };

  # copy repository contents to iso
  isoImage.contents = [{
    source = ./.;
    target = "/UpcylingAndroid";
  }]
    # copy CalyxOS factory images
    ++ lib.forEach [
      {
        url = "https://release.calyxinstitute.org/walleye-factory-22214000.zip";
        sha256 =
          "ea8ecc694e687d711efa4b571ef846fc7de85e4123883131f9ab9d75ecacc4c0";
      }
      {
        url =
          "https://release.calyxinstitute.org/blueline-factory-22309000.zip";
        sha256 =
          "96d27caf3ed4e3be5dc922da6c406a54a32bb3c0f6a52c89a4f6d5cdd9a38e49";
      }
      {
        url =
          "https://release.calyxinstitute.org/crosshatch-factory-22309000.zip";
        sha256 =
          "d5660f7ba8ae2312380f86214ee6c282d50130684781a67fd3f27d27cc18cc33";
      }
      {
        url = "https://release.calyxinstitute.org/sargo-factory-22309000.zip";
        sha256 =
          "3d3995180c7abd482b05c68ef534a08b4e5d2ee801db150a07e36345748a9f90";
      }
    ] (url: {
      source = builtins.fetchurl url;
      target = "/UpcylingAndroid/Google/CalyxOS/${builtins.baseNameOf url.url}";
    });
}
