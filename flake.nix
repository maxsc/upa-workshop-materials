{
  description = "Upcycling Android workshop materials";

  inputs= {
    nixpkgs.url = "nixpkgs/nixos-22.11";
    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, nixos-generators, ... }:
    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
    in {
      packages.x86_64-linux = {
        image = nixos-generators.nixosGenerate {
          system = "x86_64-linux";
          modules = [
            ./image.nix
          ];
          format = "iso";
        };
        default = self.packages.x86_64-linux.image;
      };
    };
}
